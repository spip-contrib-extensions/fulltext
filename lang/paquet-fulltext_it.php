<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-fulltext?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'fulltext_description' => 'Questo plugin permette da un lato di sfruttare la modalità di ricerca FULLTEXT di MySQL e quindi di migliorare enormemente le ricerche rispetto al funzionamento nativo di SPIP, e dall’altro di indicizzare il contenuto di determinati documenti.
_ Per un funzionamento ottimale, sono necessari alcuni programmi aggiuntivi sul server e devono essere configurati in <code>mes_options.php</code> o tramite il pannello di controllo.
_ Consulta la documentazione del plugin per maggiori dettagli.',
	'fulltext_slogan' => 'Indicizzazione FULLTEXT per velocizzare le ricerche'
);
