# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 2.1.3 - 2025-01-21

### Added

- #1 : possibilité de déclarer des fonctions spécifiques pour les jointures sur les recherches fulltext

### Fixed

- depreciated en PHP 8.3

## 2.1.2 - 2024-08-03

### Added

- Composerisation du plugin
